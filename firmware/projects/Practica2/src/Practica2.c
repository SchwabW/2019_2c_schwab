
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Practica2.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "delay.h"
#include "led.h"
#include "analog_io.h"
#include "string.h"
#include "uart.h"
#include "timer.h"
#include "stdio.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
int8_t contador;
uint16_t valor;
uint16_t base;
uint16_t presion;
uint16_t presion_max=0;
bool fin_conversion = false; //bandera para saber cuando termino la conversion
bool read_flanco=false;
bool prendido=false;

//si oprime tecla1
void Prender (void) // hago if de si prendido=true en las funciones, para que de ser false, este apagado el programa
{
	prendido=true;
	LedOn(LED_3); //sistema prendido
	LedOff(LED_2);
	}
//si oprime tecla 2
void Apagar (void)
{
	prendido=false;
	LedOn(LED_2); //sistema apagado
	LedOff(LED_3);

}

void flanco (void) //interrupcion para saber si esta en flanco descendente y bandera para luego empezzar a adquirir
{
	if (prendido==true)
	{
			read_flanco=true;
			TimerStart(TIMER_A);//prendo el timer para que comience a adquirir

	}
}

void fin_de_conversion (void) //interrupcion para leer el valor de gpio7 e informar por bandera si termino de convertir
{
	if (prendido==true)
	{
		AnalogInputRead(CH1,&valor);
		fin_conversion = true;
	}
}

void adquirir (void) // interrupcion paara adquirir los datos de conversion a una fm de 100 Hz
{
	if (prendido==true)
	{
		if (read_flanco==true){
		if(contador<100)
		{
			AnalogStartConvertion();
		}
		else
		{
			TimerStop(TIMER_A); //apago el timer para que solo adquiera por 1 segundo
			read_flanco=false;
			contador=0;
		}
		}
	}
}

uint16_t convertir_volt_a_presion (uint16_t  valor) //funcion que convierte los volts en mmHg recibe el valor en volt y retorna en mm Hg
{
	if (prendido==true)
	{
		uint16_t factor=3.3/(3.3-valor);
		presion= (125*factor - 75)/factor;
	}
	return (presion);
}

serial_config active_serial = {SERIAL_PORT_RS485, 115200, NULL}; //llamo a flanco para ver cuando hay un flanco descendente
timer_config active_adq = {TIMER_A,10,&adquirir}; //timer A, con 10 porque la frecuencia de muestreo es 100


int main(void)
{

	analog_input_config inputs= {CH1, AINPUTS_SINGLE_READ, &fin_de_conversion};
	UartInit(&active_serial);
	TimerInit(&active_adq);
	gpio_t pins[7];
	SystemClockInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	GPIOInit(GPIO7, GPIO_INPUT); //lo inicializo y configuro como entrada
	GPIOActivInt(GPIOGP7, GPIO7, &flanco, 0);
	LedsInit();
	LedOn(LED_2); //prendo led rojo para indicar que el sistema esta apagadp

	AnalogInputInit(&inputs);
	SwitchActivInt(SWITCH_1, Prender);
	SwitchActivInt(SWITCH_2, Apagar);
	while (1)
	{
		if (prendido==true){
			if (fin_conversion==true)
			{

				if (contador < 100)
				{
					presion=convertir_volt_a_presion(valor);
					if(presion_max<presion) //Me muestra el máximo valor de volúmen dentro de 1 segundo
					{
						presion_max=presion;
					}
					contador ++;
				}
				else
				{
					ITSE0803DisplayValue(presion_max); //muestro por display el maximo valor de presion cada 1 segundo
					UartSendString(SERIAL_PORT_RS485, UartItoa(presion, base));
					UartSendBuffer(SERIAL_PORT_RS485, "mmHg\n\r", strlen ("mmHg\n\r"));
					fin_conversion=false;
					contador=0;
					presion_max = 0;
				}

			}

		}
	}

	return 0;

}




    

/*==================[end of file]============================================*/

