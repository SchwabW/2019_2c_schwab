
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Practica.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "delay.h"
#include "led.h"
#include "analog_io.h"
#include "string.h"
#include "uart.h"
#include "timer.h"
#include "stdio.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
int8_t contador;
uint16_t temp;
uint16_t temp_acumulada;
uint16_t promedio_temp;
uint16_t temp_max=0;
uint16_t temp_min=200; // supongo este valor como el máximo del rango del sensor;
bool fin_conversion = false; //bandera para saber cuando termino la conversion
bool fin_leer = false; // bandera para saber si presiono una tecla
uint8_t value_read;
void fin_de_conversion (void) //interrupcion para leer el valor analogico por canal 1 e informar por bandera si termino de convertir
{
	AnalogInputRead(CH1,&temp);
	fin_conversion = true;
}

void adquirir (void) // interrupcion paara adquirir los datos de conversion a una fm de 20 Hz
{
		AnalogStartConvertion();
		}

void tecla (void) //interrupcion para leer la tecla e informar una bandera de que termino de leer para comenxar dentro del whilw a comparar las teclas y realizar lo que corresponda al enunciado
{
	UartReadByte(SERIAL_PORT_RS485, &value_read);
	fin_leer=true;
	}

serial_config active_serial = {SERIAL_PORT_RS485, 115200, &tecla}; //Inicializo la estructura con la que luego llamo a uartinit
timer_config active_adq = {TIMER_A,50,&adquirir}; //timer A, con 50 porque la frecuencia de muestreo es 200


int main(void)
{

	analog_input_config inputs= {CH1, AINPUTS_SINGLE_READ, &fin_de_conversion};
	UartInit(&active_serial);
	TimerInit(&active_adq);
	TimerStart(TIMER_A);
	gpio_t pins[7];
	SystemClockInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	LedsInit();
	AnalogInputInit(&inputs);

while (1)
{
	if (fin_conversion==true)
	{
		if (contador < 20)
								{
								temp_acumulada=temp+temp_acumulada;
								if(temp_max<temp) //Me muestra el máximo valor de volúmen dentro de 1 segundo
									{
										temp_max=temp;
									}
								if(temp_min>temp) //Me muestra el máximo valor de volúmen dentro de 1 segundo
									{
										temp_min=temp;
									}

								contador ++;
								}
								else
								{
									promedio_temp=temp_acumulada/contador;

									if (fin_leer==true) // se presiono una tecla
									{
									/*si quiero mostrar:
									 * PROMEDIO con tecla 'P'
									 * MAXIMO con tecla 'S'
									 * MINIMO con tecla 'I'
									 * */
										if (value_read==80) //80 es P en codigo ASCII
										{
											ITSE0803DisplayValue(promedio_temp); //muestro por display el promedio, ya que presiono la tecla 'P'
											LedOn(LED_3); //prendo el led verde, porque muestro el promedio
											LedOff(LED_2);
											LedOff(LED_1);
											fin_leer=0;
											fin_conversion=0;
										}
										else if (value_read==83) //83 es S en codigo ASCII
											{
												ITSE0803DisplayValue(temp_max);//muestro el maximo por display, ya que se presiono la tecla 'S'
												LedOn(LED_2); // prendo el led rojo, porque muestro el maximo
												LedOff(LED_1);
												LedOff(LED_3);
												fin_leer=0;
												fin_conversion=0;
											}
											else if (value_read==73) // 73 es I en codigo ASCII
												{
													ITSE0803DisplayValue(temp_min);//muestro el minimo por display, ya que se presiono la tecla 'I'
													LedOn(LED_1); // prendo el led amarillo, porque muestro el maximo
													LedOff(LED_2);
													LedOff(LED_3);
													fin_leer=0;
													fin_conversion=0;
												}

									}
									contador=0;
									temp_acumulada = 0;
									temp_max = 0;
									temp_min = 200;
								}


	}


}
return 0;

}




    

/*==================[end of file]============================================*/

