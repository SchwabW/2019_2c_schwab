Parcial 28/10/19

Consignas de Aplicación


Diseñar e implementar el firmware de un sistema de adquisición que cumpla con los siguientes requerimientos:

A) Digitalizar señal de un sensor de presión conectado al canal CH1 de la EDU-CIAA  tomando muestras a 250Hz por interrupciones.
 El sensor de presión se comporta de manera lineal, con una salida de 0V para 0 mmHg y 3.3V para 200 mmHg. 
 -------------------------------------------------------------------------------------------------------------------------------
 Para la conversión de  V a mmHg. utilizo interpolación, ya que no recuerdo la forma de realizar el cálculo teniendo en cuenta 
 la cantidad de bits del conversor. La función recibe un uint16_t valor en volts, lo convierte usando interpolacion yn retorna un uint16_t 
 presión con el  valor en mmHg 
 
Conexionado del sensor:
	sensor 		  | EDU-CIIA

salida del sensor | CH1
vcc sensor		  | vccd
gnd sensor		  | gndd
 --------------------------------------------------------------------------------------------------------------------------------

B)Obtener el valor máximo, el valor mínimo y el promedio del último segundo de señal.



C)Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a presentar en un display LCD (máximo, mínimo o promedio).
El mismo valor se debe enviar a través de la UART a la PC (en ascii).
------------------------------------------------------------------------------------------------
Para mostrar máximo presionar la tecla 1 de EDU-CIAA
Para mostrar minimo presionar la tecla 2 de EDU-CIAA
Para mostrar promedio presionar la tecla 3 de EDU-CIAA
Realice tres interrupciones que seran llamadas por la tecla 1,2 o 3 y en las mismas levanto una bandera para indicar que la 
tecla fue presionada, con la cual luego pregunto en el main y muestro segun la tecla y valor correspiondiente a mostrar. 
Dentro de la intrerrupción tambien prendo el led correspondiente segun el valor que se mostrara (PUNTO D)
-------------------------------------------------------------------------------------------------

D) El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar en el LCD 
(rojo: máximo, amarillo: mínimo, verde: promedio).



E)Si la presión máxima supera los 150 mmHg se deberá encender el red rojo del led RGB de la EDU-CIAA. 
Entre 150 y 50 mmHg el led azul y por debajo de 50 mmHg el led verde.