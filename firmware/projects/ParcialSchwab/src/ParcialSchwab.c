
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Autor: Walquiria Schwab*/

/*==================[inclusions]=============================================*/
#include "../../ParcialSchwab/inc/ParcialSchwab.h"       /* <= own header */

#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "led.h"
#include "analog_io.h"
#include "string.h"
#include "uart.h"
#include "timer.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
int8_t contador;
uint16_t valor; //el valor que recibe en volts
uint16_t base=10; //para mostrar en decimal
uint16_t presion;
uint16_t presion_max=0;
uint16_t presion_min=200; //fijo el valor de la maxima presión posible
uint16_t promedio_presion;
uint16_t presion_acumulada=0;
bool fin_conversion = false; //bandera para saber cuando termino la conversion
bool bandera_maximo=false; //bandera para saber si apreto la tecla de para mostrar maximo
bool bandera_minimo=false; //bandera para saber si apreto la tecla de para mostrar minimo
bool bandera_promedio=false; //bandera para saber si apreto la tecla de para mostrar promedio


//Si oprime la tecla 1 de la EDU-CIAA se mostrará el máximo valor de presión
void Maximo (void) // hago if de si prendido=true en las funciones, para que de ser false, este apagado el programa
{
	bandera_maximo=true;
	LedOn(LED_2); //prendo el led rojo opara indicar que estoy mostrando el máximo valor de presión
	LedOff(LED_1);
	LedOff(LED_3);
}

//Si oprime la tecla 2 de la EDU-CIAA se mostrará el mínimo valor de presión
void Minimo (void)
{
	bandera_minimo=true;
	LedOn(LED_1); //prendo el led amarillo opara indicar que estoy mostrando el minimo valor de presión
	LedOff(LED_2);
	LedOff(LED_3);

}

//Si oprime la tecla 3 de la EDU-CIAA se mostrará el valor promedio de presión
void Promedio (void)
{
	bandera_promedio=true;
	LedOn(LED_3); //prendo el led verde para indicar que estoy mostrando el valor promedio de presión
	LedOff(LED_1);
	LedOff(LED_2);
}


void fin_de_conversion (void) //interrupcion para leer el valor de gpio7 e informar por bandera si termino de convertir
{

	AnalogInputRead(CH1,&valor); //leo el valor por el canal CH1 y lo guardo en uint16_t valor
	fin_conversion = true; // levanto bandera de fin de conversión
}

void adquirir (void) // interrupcion paara adquirir los datos a una fm de 250 Hz
{
	AnalogStartConvertion();
}



uint16_t convertir_volt_a_presion (uint16_t  valor) //funcion que convierte los volts en mmHg recibe el valor en volts y retorna en mm Hg
{

	uint16_t factor=3.3/(3.3-valor);
	presion= (200*factor - 200)/factor;

	return (presion);
}

serial_config active_serial = {SERIAL_PORT_RS485, 115200, NULL}; //para inicializar UART
timer_config active_adq = {TIMER_A,4,&adquirir}; //timer A, con 4 porque la frecuencia de muestreo es 250Hz


int main(void)
{

	analog_input_config inputs= {CH1, AINPUTS_SINGLE_READ, &fin_de_conversion};
	UartInit(&active_serial);
	TimerInit(&active_adq);
	TimerStart(TIMER_A);
	gpio_t pins[7];
	SystemClockInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	LedsInit();
	AnalogInputInit(&inputs);
	SwitchActivInt(SWITCH_1, Maximo);
	SwitchActivInt(SWITCH_2, Minimo);
	SwitchActivInt(SWITCH_3, Promedio);
	while (1)
	{
		if (fin_conversion==true)
		{

			if (contador < 250)
			{
				presion=convertir_volt_a_presion(valor);
				presion_acumulada=presion+presion_acumulada;
				if(presion_max<presion) //Me guarda el maximo valor de presion en 1  segundo
				{
					presion_max=presion;
				}
				if(presion_min>presion) //Me guarda el minimo valor de presion dentro de 1 segundo
				{
					presion_min=presion;
				}
				contador ++;
			}
			else
			{
				promedio_presion=presion_acumulada/contador;

				if (bandera_maximo==true){
					ITSE0803DisplayValue(presion_max); //muestro por display el maximo valor de presion cada 1 segundo
					UartSendString(SERIAL_PORT_RS485, UartItoa(presion_max, base)); //envio a la PC el valor maximo de presion cada 1 segundo
					UartSendBuffer(SERIAL_PORT_RS485, "mmHg\n\r", strlen ("mmHg\n\r"));
					fin_conversion=false;
					bandera_maximo=false;
				}
				else if (bandera_minimo==true){
					ITSE0803DisplayValue(presion_min); //muestro por display el minimo valor de presion cada 1 segundo
					UartSendString(SERIAL_PORT_RS485, UartItoa(presion_min, base)); //envio a la PC el valor minimo de presion cada 1 segundo
					UartSendBuffer(SERIAL_PORT_RS485, "mmHg\n\r", strlen ("mmHg\n\r"));
					fin_conversion=false;
					bandera_minimo=false;
				}
				else if (bandera_promedio==true){
					ITSE0803DisplayValue(promedio_presion); //muestro por display el valor promedio de presion cada 1 segundo
					UartSendString(SERIAL_PORT_RS485, UartItoa(promedio_presion, base)); //envio a la PC el valor promedio de presion cada 1 segundo
					UartSendBuffer(SERIAL_PORT_RS485, "mmHg\n\r", strlen ("mmHg\n\r"));
					fin_conversion=false;
					bandera_promedio=false;
				}

				contador=0; //inicializo el contador
				presion_acumulada=0; // inicializo presión acumulada, para siempre tener acumuladas las 250 muestras que estan dentro del segundo y que no haya acumulacion de anteriores
				presion_max=0; //iniclializo presion max
				presion_min=200; //iniclializo presion max
			}

			if (presion_max>=150)
			{
				LedOn(LED_RGB_R); // prendo el led rojo si la presion maxima es mayor a 150 mm Hg
				LedOff(LED_RGB_G);
				LedOff(LED_RGB_B);
			}
			if ((presion_max>50)&&(presion_max<150))
			{
				LedOn(LED_RGB_B); //prendo el led azul si la presion maxima se encuentra dentro de un rango de 50  a 150 mm Hg
				LedOff(LED_RGB_R);
				LedOff(LED_RGB_G);
			}
			else
			{
				LedOn(LED_RGB_G); //prendo el led verde si la presion maxima es menor a 50 mm Hg
				LedOff(LED_RGB_B);
				LedOff(LED_RGB_R);
			}
		}

	}



	return 0;

}




    

/*==================[end of file]============================================*/

