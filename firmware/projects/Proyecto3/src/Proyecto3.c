
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3.h"       /* <= own header */

#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "string.h"
#include "uart.h"
#include "timer.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
//bool active_key;

bool key_aux_1=false;
bool key_aux_2=false;
bool key_aux_3=false;
bool key_aux_4=false;
uint8_t base = 10;
//int16_t distance_cm;
int16_t distance;
bool distance_cm_bool=false;
bool Hold_cm=false, Hold_inches=false;//tecla para manetener el valor
void Activate_stop_measurement (void)
{
	key_aux_1^=true;
	}
void Hold (void)
{
	key_aux_2=true;
}
void Measurement_cm (void)
{
	key_aux_3=true;
}

void Measurement_inches (void)
{
	key_aux_3=false;
}

serial_config active_serial = {SERIAL_PORT_RS485, 115200, NULL}; //Inicializo la estructura con la que luego llamo a uartinit
void refresh (void)
{

	Measurement_cm();
	UartSendString(SERIAL_PORT_RS485, UartItoa(distance, base));
	UartSendBuffer(&active_serial, "cm\n\r", strlen ("cm\n\r"));

}

timer_config active_timer = {TIMER_A,1000,&refresh}; //timer B porque es el que anda, 100, porque el timer es de 10 ms y necesito un seg


int main(void)
{
	UartInit(&active_serial);

	TimerInit(&active_timer);
	TimerStart(TIMER_A);
	int8_t value_read=SwitchesRead();
	SystemClockInit();
//	uint16_t valor=0;
	gpio_t pins[7];
	SystemClockInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	SwitchesInit();//inicializa el drivers tecla
	//int8_t key;

	//bool Distance;
	HcSr04Init(T_FIL2, T_FIL3); //llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
	while (1)
	{
	DelayMs(200);
		if (key_aux_1==true){

		if (key_aux_3==true){
			distance=HcSr04ReadDistanceCentimeters();//devuelve la distancia en cm
			}
			else  {
				distance=HcSr04ReadDistanceInches();

			}
		if (key_aux_2==false){
			ITSE0803DisplayValue(distance);
			}

		}
		else {
			ITSE0803DisplayValue(0);
			}
	}
		//key=SwitchesRead(); //lee la tecla

		SwitchActivInt(SWITCH_1, Activate_stop_measurement);
		SwitchActivInt(SWITCH_2, Hold);
		SwitchActivInt(SWITCH_3, Measurement_cm);
		SwitchActivInt(SWITCH_4, Measurement_inches);
		/* switch (key)
	{case SWITCH_1: //para activar y detener la medición
		key_aux_1^=true;

	break;
	case SWITCH_2: //para mantener el resultado (“HOLD”)
		key_aux_2=true;

	break;
	case SWITCH_3: //para configurar la medición en cm.
		key_aux_3=true;

	break;
	case SWITCH_4: //para configurar la medición en pulgadas.
		key_aux_4=true;

	break;
	} */




return 0;

}


/*Proyecto: Medidor de distancia por ultrasonido
Mostrar distancia medida por LCD.
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).
Usar TEC3 para configurar la medición en cm.
Usar TEC4 para configurar la medición en plg.

* Usar INTERRUPCIONES para el manejo de las teclas.*/


    

/*==================[end of file]============================================*/

