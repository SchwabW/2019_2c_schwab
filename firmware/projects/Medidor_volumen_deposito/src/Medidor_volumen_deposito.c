
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_volumen_deposito.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "led.h"
#include "analog_io.h"
#include "ili9341.h"
#include "spi.h"
#include "string.h"
#include "uart.h"
#include "timer.h"
#include "stdio.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
uint8_t base = 10;
int16_t distance;
int16_t volumen;
int16_t volumen_acumulado;
int16_t promedio_vol;
int8_t contador;
#define PI 3.14159
#define radio 5
#define area PI*radio*radio
#define distance_max 20
#define vol_max area*distance_max
char data_vol [10]= "VOLUMEN";
char data_temp [20]= "TEMPERATURA";
char unidad_vol [10]= "cc";
char unidad_temp [10]= "grados";
uint16_t foreground=50;
uint16_t background=20;
Font_t font_11x18;
uint16_t temp;
uint16_t temp_acumulada;
uint16_t promedio_temp;
bool fin_conversion = false;
int16_t volumen_max=0;
void fin_de_conversion (void)
{
	AnalogInputRead(CH1,&temp);
	fin_conversion = true;

}
void adquirir (void) //
{

		AnalogStartConvertion();


		}


serial_config active_serial = {SERIAL_PORT_RS485, 115200, NULL}; //Inicializo la estructura con la que luego llamo a uartinit
timer_config active_adq = {TIMER_A,250,&adquirir}; //timer B porque es el que anda, 100, porque el timer es de 10 ms y necesito un seg




/*void refresh (void)
{
	LedToggle(LED_RGB_B);
	UartSendString(SERIAL_PORT_RS485, UartItoa(promedio_vol, base));
	UartSendBuffer(&active_serial, "cm\n\r", strlen ("cm\n\r"));

}*/

int main(void)
{

	analog_input_config inputs= {CH1, AINPUTS_SINGLE_READ, &fin_de_conversion};
	UartInit(&active_serial);
	TimerInit(&active_adq);
	TimerStart(TIMER_A);

	//int8_t value_read=SwitchesRead();
	SystemClockInit();
	LedsInit();
	ILI9341Init(SPI_1,GPIO5 ,GPIO3, GPIO1); //GPIO5 cs, GPIO3 dc, GPIO1 rst
	HcSr04Init(T_FIL2, T_FIL3); //llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
	AnalogInputInit(&inputs);
while (1)
{
	if (fin_conversion==true)
	{
		distance=HcSr04ReadDistanceCentimeters();//devuelve la distancia en cm del sensor (colocado en el borde superior) hasta la interfaz de agua
		if (distance>distance_max)
		{
			distance=distance_max;
		}
		volumen=area*(distance_max - distance);

		if (contador < 4)
								{
								volumen_acumulado=volumen+volumen_acumulado;
								temp_acumulada=temp+temp_acumulada;
								if(volumen_max<volumen) //Me muestra el máximo valor de volúmen dentro de 1 segundo
									{
										volumen_max=volumen;
									}
								contador ++;
								}
								else
								{
									promedio_vol=volumen_acumulado/contador;
									promedio_temp=temp_acumulada/contador;
									ILI9341DrawString(20, 50, data_vol,&font_11x18 , foreground, background);
									ILI9341DrawInt(20, 100, promedio_vol, 5, &font_11x18, foreground, background);
									ILI9341DrawString(120, 100, unidad_vol,&font_11x18 , foreground, background);
									ILI9341DrawString(20, 150, data_vol,&font_11x18 , foreground, background);
									ILI9341DrawInt(20, 200, promedio_temp, 5, &font_11x18, foreground, background);
									ILI9341DrawString(120, 200, unidad_temp,&font_11x18 , foreground, background);
									UartSendString(SERIAL_PORT_RS485, UartItoa(promedio_vol, base));
									UartSendBuffer(SERIAL_PORT_RS485, "cmcubicos\n\r", strlen ("cmcubicos\n\r"));
									UartSendString(SERIAL_PORT_RS485, UartItoa(promedio_temp, base));
									UartSendBuffer(SERIAL_PORT_RS485, "grados\n\r", strlen ("grados\n\r"));
									contador=0;
									volumen_acumulado = 0;
								}

			if (promedio_vol<=(0.3*vol_max))
			{
				LedOn(LED_RGB_R);
				LedOff(LED_RGB_G);
				LedOff(LED_RGB_B);
			}
			if (promedio_vol>(0.3*vol_max)&&promedio_vol<(0.7*vol_max))
			{
				LedOn(LED_RGB_G);
				LedOff(LED_RGB_R);
				LedOff(LED_RGB_B);
			}
			else
			{
				LedOn(LED_RGB_B);
				LedOff(LED_RGB_G);
				LedOff(LED_RGB_R);
			}



fin_conversion=0;
	}


}
return 0;

}




    

/*==================[end of file]============================================*/

