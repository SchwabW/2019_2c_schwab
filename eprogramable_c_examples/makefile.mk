########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = GUIA1_7
#NOMBRE_EJECUTABLE = ejercicio7.exe

#PROYECTO_ACTIVO = GUIA1_9
#NOMBRE_EJECUTABLE = ejercicio9.exe

#PROYECTO_ACTIVO = GUIA1_12
#NOMBRE_EJECUTABLE = ejercicio12.exe

#PROYECTO_ACTIVO = GUIA1_14
#NOMBRE_EJECUTABLE = ejercicio14.exe


#PROYECTO_ACTIVO = GUIA1_16
#NOMBRE_EJECUTABLE = ejercicio16.exe

#PROYECTO_ACTIVO = GUIA1_INTEGRADOR_A
#NOMBRE_EJECUTABLE = integrador_a.exe

#PROYECTO_ACTIVO = GUIA1_INTEGRADOR_C
#NOMBRE_EJECUTABLE = integrador_c.exe

PROYECTO_ACTIVO = GUIA1_INTEGRADOR_D
NOMBRE_EJECUTABLE = integrador_d.exe