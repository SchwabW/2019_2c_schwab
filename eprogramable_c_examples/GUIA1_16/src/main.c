/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
//Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
//Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
//cargue cada uno de los bytes de la variable de 32 bits.
//Realice el mismo ejercicio, utilizando la definición de una “union”.

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/


int main(void)
{
   uint32_t A;

   union test{
       struct {
           uint8_t byte1;
           uint8_t byte2;
           uint8_t byte3;
           uint8_t byte4;
       } cada_byte;
       uint32_t  todos_los_bytes;
   } U;


   A=0x01020304;
   uint8_t B, C, D ,E;
   U.todos_los_bytes= A;

   E= A; // copio los 8 bits menos significativos
   D = A>>8; // corro los 8 bits sigueintes y vuelvo a copiar
   C = A>>16;
   B = A>>24;

   printf("%d es A\r\n",A);
   printf("%d es B\r\n",B);
   printf("%d es C\r\n",C);
   printf("%d es D\r\n",D);
   printf("%d es E\r\n",E);


   	printf("%d es B_union\r\n",U.cada_byte.byte4);
   	printf("%d es C_union\r\n",U.cada_byte.byte3);
   	printf("%d es D_union\r\n",U.cada_byte.byte2);
   	printf("%d es E_union\r\n",U.cada_byte.byte1);



	return 0;
}

/*==================[end of file]============================================*/

