/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
//Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de
//20 caracteres y edad.  a. Defina una variable con esa estructura y cargue los campos con
//sus propios datos. b. Defina un puntero a esa estructura y cargue los campos con los datos de
//su compañero (usando acceso por punteros).

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
struct alumno{
	char nombre[12];
	char apellido[20];
	uint8_t edad;
} yo, compa, *punt;//creo mi struct de nombre alumno y dos variables yo y compa del tipo alumno, mas un puntero a un tipo alumno
int main(void)
{
strcpy(yo.nombre, "Walquiria");//accedo a nopmbre de la variable yo para asignarle el dato
strcpy(yo.apellido, "Schwab");
yo.edad=27;
printf("\n Mi nombre es %s ",yo.nombre);//OJO, cuando muestro char es diferente que para mostrar numeros
printf("\n Mi apellido %s ",yo.apellido);
printf("\n Mi edad es %d ",yo.edad);
punt=&compa;//asigno al puntero punt la direccion de memoria de la variable llamada compa
strcpy((*punt).nombre, "Brenda");//"Brenda"=punt->nombre es otra forma de acceder. Accedo a nombre de lo que apunta el puntero para asignarle el dato.
strcpy((*punt).apellido, "Luna Paez");//accedo a apellido de lo que apunta punt para asignarle el dato
(*punt).edad=26;//accedo a edad de lo que apunta punt para asignarle el dato
printf("\n Mi compañera se llama %s ",(*punt).nombre);
printf("\n Su apellido es %s ",compa.apellido);
printf("\n Su edad es %d ",compa.edad);
	return 0;
}

/*==================[end of file]============================================*/

